import { Component } from "react";

class FetchApi extends Component {
    fetchApi = async (url, body) => {
        let response = await  fetch(url, body);

        let json = await response.json();

        return json;
    }

    getAllApi = () => {
        this.fetchApi("https://jsonplaceholder.typicode.com/posts").then((data) => {
            console.log(data);
        })
    }

    getById = () => {
        this.fetchApi("https://jsonplaceholder.typicode.com/posts/1").then((data) => {
            console.log(data);
        })
    }

    create = () => {
        let body = {
            method: 'POST',
            body: JSON.stringify({
              title: 'foo',
              body: 'bar',
              userId: 1,
            }),
            headers: {
              'Content-type': 'application/json; charset=UTF-8',
            }
        }

        this.fetchApi("https://jsonplaceholder.typicode.com/posts", body).then((data) => {
            console.log(data);
        })
    }

    update = () => {
        let body = {
            method: 'PUT',
            body: JSON.stringify({
                id: 1,
                title: 'foo',
                body: 'bar',
                userId: 1,
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }

        this.fetchApi("https://jsonplaceholder.typicode.com/posts/1", body).then((data) => {
            console.log(data);
        })
    }

    delete = () => {
        let body = {
            method: 'DELETE'
        }

        this.fetchApi("https://jsonplaceholder.typicode.com/posts/1", body).then((data) => {
            console.log(data);
        })
    }

    render() {
        return (
            <div className="row m-5">
                <div className="col"><button className="btn btn-primary" onClick={this.getAllApi}>Get all</button></div>
                <div className="col"><button className="btn btn-primary" onClick={this.getById}>Get by Id</button></div>
                <div className="col"><button className="btn btn-primary" onClick={this.create}>Create</button></div>
                <div className="col"><button className="btn btn-primary" onClick={this.update}>Update</button></div>
                <div className="col"><button className="btn btn-primary" onClick={this.delete}>Delete</button></div>
            </div>
        )
    }
}

export default FetchApi;