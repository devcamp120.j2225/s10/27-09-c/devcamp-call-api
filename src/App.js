import "bootstrap/dist/css/bootstrap.min.css";
import AxiosLibrary from "./components/Axios";
import FetchApi from "./components/FetchAPI";

function App() {
  return (
    <div className="container">
      <FetchApi />
      <AxiosLibrary />
    </div>
  );
}

export default App;
